# Raspberry Ansible Scripts #

## Links ##

- [Github Gist | Running ARM docker image with QEMU on x86_64 Arch Linux host](https://gist.github.com/Manu343726/ca0ceb224ea789415387)
- [Github | twobitcircus/rpi-build-and-boot](https://github.com/twobitcircus/rpi-build-and-boot)
- [Filipeflop | Broker MQTT com Raspberry PI Zero W](https://www.filipeflop.com/blog/broker-mqtt-com-raspberry-pi-zero-w/)
- [Mosquitto Username and Password Authentication -Configuration and Testing](http://www.steves-internet-guide.com/mqtt-username-password-example/)
- [Setting up Authentication in Mosquitto MQTT Broker](https://medium.com/@eranda/setting-up-authentication-on-mosquitto-mqtt-broker-de5df2e29afc)
- [Getting start with MQTT Mosquitto Broker on Raspberry Pi, Windows, macOS and Linux [update]](https://diyprojects.io/mqtt-mosquitto-communicating-connected-objects-iot/#.W_C0rnVKjDc)
- [Guide to IOT Dashboards and Platforms](http://www.steves-internet-guide.com/iot-mqtt-dashboards/)
- [The Seven Best MQTT Client Tools](https://www.hivemq.com/blog/seven-best-mqtt-client-tools)
- [Awesome MQTT](https://github.com/hobbyquaker/awesome-mqtt)
