#!/bin/bash

echo "############################################"
echo "##    Starting to verify ansible roles    ##"
echo "############################################"

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
CURR_DIR=${PWD}

errors=( )

#function validate_python {
#  pip install -r $SCRIPT_DIR/requirements.txt
#}

#validate_python

RESULTS=(  )

function validate_ansible_code {
  for folder in $CURR_DIR/roles/*/
  do
    echo "- verifying ${folder}"
    if [ -d $folder/molecule ]; then
      cd $folder
      molecule test
      if [ $? -eq 0 ]; then
        RESULTS+=( "${folder}: OK" )
      else
        errors+=($folder)
        RESULTS+=( "${folder}: FAIL" )
      fi
    else
      RESULTS+=( "${folder}: NOT TESTED (doesn't have configred tests)" )
    fi
  done

}

validate_ansible_code

echo ""

echo "results:"
for result in "${RESULTS[@]}"
do
  echo "- ${result}"
done

echo ""

cd $CURR_DIR

if [ ${#errors[@]} -eq 0 ]; then
  echo "Success"
  exit 0
else
  echo "Failed"
  exit 1
fi
